package gomongodb

import (
	"fmt"

	"gitlab.com/barcos.co/gocore/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func IDFromInsertedResult(result *mongo.InsertOneResult) (string, errors.Error) {
	var someErr errors.Error
	ID, ok := result.InsertedID.(primitive.ObjectID)
	if !ok {
		someErr.SetInternalServerError()
		err := fmt.Errorf("unable to cast the inserted objecID. This error should not occur")
		someErr.PrimitiveErr = &err
		return "", someErr
	}
	return ID.Hex(), someErr.SetNilError()
}

func IDFromUpsertedResult(result *mongo.UpdateResult) (string, errors.Error) {
	var someErr errors.Error
	ID, ok := result.UpsertedID.(primitive.ObjectID)
	if !ok {
		someErr.SetInternalServerError()
		err := fmt.Errorf("unable to cast the updated objecID. This error should not occur")
		someErr.PrimitiveErr = &err
		return "", someErr
	}
	return ID.Hex(), someErr.SetNilError()
}

func VerifyObjectIDs(ids ...string) ([]*primitive.ObjectID, errors.Error) {
	var someErr errors.Error
	result := []*primitive.ObjectID{}
	for _, id := range ids {
		objectID, err := primitive.ObjectIDFromHex(id)
		if err != nil || objectID.IsZero() {
			someErr.SetBadRequest()
			someErr.PrimitiveErr = &err
			return nil, someErr
		}
		result = append(result, &objectID)
	}

	someErr.SetNilError()
	return result, someErr
}
