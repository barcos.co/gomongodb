package gomongodb

import (
	"context"

	"gitlab.com/barcos.co/gocore/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (dbClient MongoDBClient) FindOneDocumentByID(
	documentID string, documentModel interface{}, collection string) errors.Error {
	IDs, someErr := VerifyObjectIDs(documentID)
	if !someErr.IsNilError() {
		return someErr
	}

	return dbClient.FindOneDocumentByQuery(primitive.M{"_id": IDs[0]}, documentModel, collection)
}

func (dbClient MongoDBClient) FindOneDocumentByQuery(
	query map[string]interface{}, documentModel interface{}, collection string) errors.Error {
	var someErr errors.Error
	mgoCollection := dbClient.GetCollectionByName(collection)

	if len(query) == 0 {
		someErr.SetNotFound()
		return someErr
	}

	result := mgoCollection.FindOne(context.TODO(), query)
	if result.Err() == mongo.ErrNoDocuments {
		someErr.SetDocumentDontExistError()
		return someErr
	}

	if result.Err() != nil {
		someErr.SetInternalServerError(result.Err())
		return someErr
	}

	if err := result.Decode(documentModel); err != nil {
		someErr.SetInternalServerError()
		return someErr
	}

	if result == nil {
		dbClient.LogInfof("find document fail.      collection=%s document_id=%v\n", collection, query)
	} else {
		dbClient.LogInfof("find document success.   collection=%s document_id=%v\n", collection, query)
	}

	return someErr.SetNilError()
}

func (dbClient *MongoDBClient) FindManyDocumentsByID(documentIDs []string, documentModel interface{}, collection string) errors.Error {
	objectIDs, someErr := VerifyObjectIDs(documentIDs...)
	if !someErr.IsNilError() {
		return someErr
	}
	filter := make(map[string]interface{})
	filter["_id"] = primitive.M{"$in": objectIDs}
	return dbClient.FindManyDocuments(documentModel, collection, filter)
}

func (dbClient *MongoDBClient) FindManyDocuments(documentModel interface{}, collection string, query map[string]interface{},
	opts ...*options.FindOptions) errors.Error {

	var someErr errors.Error
	mgoCollection := dbClient.GetCollectionByName(collection)

	cursor, err := mgoCollection.Find(context.Background(), query, opts...)
	if err != nil {
		someErr.SetInternalServerError()
		someErr.PrimitiveErr = &err
		return someErr
	}

	size := cursor.RemainingBatchLength()

	if err := cursor.All(context.TODO(), documentModel); err != nil {
		someErr.SetInternalServerError()
		someErr.PrimitiveErr = &err
		return someErr
	}

	if size < 0 {
		dbClient.LogInfof("0 document found.      collection=%s query=%v\n", collection, query)
	} else {
		str := ""
		if size > 1 {
			str = "s"
		}
		dbClient.LogInfof("%v document%s found.   collection=%s query=%v\n", size, str, collection, query)
	}

	return someErr.SetNilError()
}
