package gomongodb

import (
	"context"
	"strings"

	"gitlab.com/barcos.co/gocore/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

func (dbClient MongoDBClient) SearchByText(
	searchText string, documents interface{}, collection string, withProjection bool, projection primitive.M) errors.Error {
	var someErr errors.Error

	matchFilter := bson.M{}
	aggregationPipeline := mongo.Pipeline{}

	matchFilter["$text"] = bson.M{"$search": strings.TrimSpace(searchText)}
	matchStage := primitive.D{primitive.E{Key: "$match", Value: matchFilter}}

	aggregationPipeline = append(aggregationPipeline, matchStage)

	if withProjection {
		aggregationPipeline = append(aggregationPipeline, primitive.D{primitive.E{Key: "$project", Value: projection}})
	}

	sortFilter := bson.M{"score": bson.M{"$meta": "textScore"}}
	sortStage := primitive.D{primitive.E{Key: "$sort", Value: sortFilter}}
	aggregationPipeline = append(aggregationPipeline, sortStage)

	cursor, err := dbClient.GetCollectionByName(collection).Aggregate(context.TODO(), aggregationPipeline)
	if err != nil {
		someErr.SetInternalServerError(err)
		return someErr
	}

	if err := cursor.All(context.TODO(), documents); err != nil {
		someErr.SetInternalServerError(err)
		return someErr
	}

	return someErr.SetNilError()
}
