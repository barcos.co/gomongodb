package gomongodb

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/barcos.co/gocore/logger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/mgo.v2/bson"
)

const AuthMechanism = "SCRAM-SHA-1"
const DefaultTimeout = 5 * time.Second

type MongoDBClient struct {
	MongoClient *mongo.Client
	Opts        MongoClientOpts
}

func NewMongoClient() *MongoDBClient {
	opts := GetMongoClientOpts()
	c := &MongoDBClient{Opts: opts}

	mainUri := fmt.Sprintf("mongodb://%s:%d", opts.GetHost(), opts.GetPort())
	credentials := options.Credential{
		Username:      opts.GetUserName(),
		Password:      opts.GetPassword(),
		AuthMechanism: AuthMechanism,
		AuthSource:    opts.GetDBName(),
	}

	rawClient := options.Client().
		ApplyURI(mainUri).
		SetAuth(credentials).
		SetConnectTimeout(DefaultTimeout)

	mc, err := mongo.NewClient(rawClient)
	if err != nil {
		log.Fatalln(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	err = mc.Connect(ctx)
	if err != nil {
		log.Fatalln(err)
	}
	defer cancel()

	err = mc.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatalln(err)
	}

	c.LogInfo("Connection success")
	c.MongoClient = mc
	return c
}

// SetMongoIndex create the index model for the users collection in order to prevent duplication of emails
func (dbClient MongoDBClient) SetMongoIndex(indexesSlice []mongo.IndexModel, collectionName string) {

	var indexes []bson.M
	indexView := dbClient.GetCollectionByName(collectionName).Indexes()
	cursor, err := indexView.List(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	if err = cursor.All(context.TODO(), &indexes); err != nil {
		log.Fatal(err)
	}

	// delete already added indexes
	for _, newValue := range indexesSlice {
		for _, extingValue := range indexes {
			newNameValue := *newValue.Options.Name
			existingNameValue := extingValue["name"]
			if newNameValue == existingNameValue {
				indexesSlice = append(indexesSlice[:0], indexesSlice[1:]...)
			}
		}
	}

	if len(indexesSlice) > 0 {
		ids, err := indexView.CreateMany(context.TODO(), indexesSlice)
		if err != nil {
			log.Fatal(err)
		}

		for _, id := range ids {
			dbClient.LogInfof("SetMongoIndex: Index %s field succesfully added on %s collection\n", id, collectionName)
		}
	}
}

func (dbClient MongoDBClient) CreateSimpleIndex(indexKey string, value int, collectionName string) {
	found := false
	cursor, err := dbClient.GetCollectionByName(collectionName).Indexes().List(context.Background())
	if err != nil {
		panic(err)
	}

	for cursor.Next(context.Background()) {
		indexValue := make(map[string]interface{})
		if err := cursor.Decode(indexValue); err != nil {
			panic(err)
		}

		if m, ok := indexValue["key"].(map[string]interface{}); ok && m[indexKey] != nil {
			found = true
		}
	}

	if !found {
		dbClient.LogInfof("Injecting Index %s=%v...\n", indexKey, value)
		dateIndexModel := mongo.IndexModel{Keys: bson.M{indexKey: value}}
		dbClient.GetCollectionByName(collectionName).Indexes().CreateOne(context.Background(), dateIndexModel)
	}
}

func (dbClient MongoDBClient) GetCollectionByName(collection string) *mongo.Collection {
	var col *mongo.Collection

	if db := dbClient.MongoClient.Database(dbClient.Opts.GetDBName()); db != nil {
		col = db.Collection(collection)
	} else {
		panic(errors.New("unable to get dbName " + dbClient.Opts.GetDBName()))
	}

	return col
}

func (dbClient MongoDBClient) LogInfof(format string, a ...interface{}) {
	logger.FLogInfof(log.Default().Writer(), getPrefix(), format, a...)
}

func (dbClient MongoDBClient) LogInfo(a ...interface{}) {
	logger.FLogInfo(log.Default().Writer(), getPrefix(), a...)
}

func getPrefix() string {
	return "MongoClient"
}
