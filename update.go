package gomongodb

import (
	"context"

	"gitlab.com/barcos.co/gocore/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (dbClient MongoDBClient) UpdateOneDocumentByID(
	documentID string, update map[string]interface{}, collection string) errors.Error {
	IDs, someErr := VerifyObjectIDs(documentID)
	if !someErr.IsNilError() {
		return someErr
	}
	return dbClient.UpdateOneDocumentByQuery(primitive.M{"_id": IDs[0]}, update, collection)
}

func (dbClient MongoDBClient) UpdateOneDocumentByQuery(
	query map[string]interface{}, update map[string]interface{}, collection string) errors.Error {
	var someErr errors.Error

	succes := "success"
	mgoCollection := dbClient.GetCollectionByName(collection)

	result, err := mgoCollection.UpdateOne(context.TODO(), query, update)
	if mongo.IsDuplicateKeyError(err) {
		someErr.SetDocumentAlreadyExistError()
		return someErr
	}

	if err != nil {
		someErr.SetForbidden()
		someErr.PrimitiveErr = &err
		return someErr
	}

	if result.ModifiedCount != 1 {
		succes = "wrong"
	}

	dbClient.LogInfof("update document %s.    collection=%s query=%v update=%v\n", succes, collection, query, update)
	someErr.SetNilError()
	return someErr
}
