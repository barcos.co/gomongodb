package gomongodb

import (
	"context"

	"gitlab.com/barcos.co/gocore/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

func (dbClient MongoDBClient) InsertOneDocument(
	document interface{}, collection string) (string, errors.Error) {
	var someErr errors.Error
	mgoCollection := dbClient.GetCollectionByName(collection)

	if document == nil {
		someErr.SetBadRequest()
		return "", someErr
	}

	result, err := mgoCollection.InsertOne(context.TODO(), document)
	if mongo.IsDuplicateKeyError(err) {
		someErr.SetDocumentAlreadyExistError()
		return "", someErr
	}

	if err != nil {
		someErr.SetInternalServerError()
		someErr.PrimitiveErr = &err
		return "", someErr
	}

	ID, someErr := IDFromInsertedResult(result)

	if ID == "" {
		dbClient.LogInfof("insert document fail.    collection=%s document_id=%v\n", collection, ID)
	} else {
		dbClient.LogInfof("insert document success. collection=%s document_id=%v\n", collection, ID)
	}

	if !someErr.IsNilError() {
		return "", someErr
	}

	someErr.SetNilError()
	return ID, someErr
}
