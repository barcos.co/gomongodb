package gomongodb

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/barcos.co/gocore/logger"
)

var mongoConfigs *MongoClientOpts

const (
	MONGO_DB_PORT_ENV     = "MONGO_DB_PORT"
	MONGO_DB_HOST_ENV     = "MONGO_DB_HOST"
	MONGO_DB_USERNAME_ENV = "MONGO_DB_USERNAME"
	MONGO_DB_PASSWORD_ENV = "MONGO_DB_PASSWORD"
	MONGO_DB_NAME_ENV     = "MONGO_DB_NAME"
)

type MongoClientOpts struct {
	host     *string
	port     *int
	userName *string
	password *string
	dBName   string
}

func LoadMongoSettings() {
	GetMongoClientOpts()
}

func GetMongoClientOpts() MongoClientOpts {
	if mongoConfigs == nil {
		s := getMongoSettings()
		mongoConfigs = &s
		mongoConfigs.display()
	}
	return *mongoConfigs
}

func (s *MongoClientOpts) display() {
	prefix := "Mongo Config Read"
	logger.FLogInfof(log.Default().Writer(), prefix, "Host     \t\t --> %s \n", s.GetHost())
	logger.FLogInfof(log.Default().Writer(), prefix, "Port     \t\t --> %v \n", s.GetPort())
	logger.FLogInfof(log.Default().Writer(), prefix, "UserName \t\t --> %s \n", s.GetUserName())
	logger.FLogInfof(log.Default().Writer(), prefix, "Password   \t\t --> %s \n", s.GetPassword())
	logger.FLogInfof(log.Default().Writer(), prefix, "DBName   \t\t --> %s \n\n", s.GetDBName())
}

func (s *MongoClientOpts) GetHost() string {
	if s.host != nil {
		return *s.host
	}
	return ""
}

func (s *MongoClientOpts) GetPort() int {
	if s.port != nil {
		return *s.port
	}
	return 0
}

func (s *MongoClientOpts) GetUserName() string {
	if s.userName != nil {
		return *s.userName
	}
	return ""
}

func (s *MongoClientOpts) GetPassword() string {
	if s.password != nil {
		return *s.password
	}
	return ""
}

func (s *MongoClientOpts) GetDBName() string {
	return s.dBName
}

func getMongoSettings() MongoClientOpts {
	var port int
	var errorMessage string

	port, err := strconv.Atoi(os.Getenv(MONGO_DB_PORT_ENV))
	if err != nil {
		errorMessage = fmt.Sprintf("Error while loading %v", MONGO_DB_PORT_ENV)
		fmt.Fprintln(os.Stderr, errorMessage)
		panic(errorMessage)
	}

	if port == 0 {
		errorMessage = fmt.Sprintf("Error while loading %v. Ensure a valid port number", MONGO_DB_PORT_ENV)
		panic(errorMessage)
	}

	host := os.Getenv(MONGO_DB_HOST_ENV)
	userName := os.Getenv(MONGO_DB_USERNAME_ENV)
	password := os.Getenv(MONGO_DB_PASSWORD_ENV)
	dbName := os.Getenv(MONGO_DB_NAME_ENV)

	return MongoClientOpts{host: &host, port: &port, userName: &userName, password: &password, dBName: dbName}
}
